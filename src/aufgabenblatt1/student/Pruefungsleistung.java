package aufgabenblatt1.student;

/**
* Praktikum TIPR2, SS 2015
* Gruppe: Soufian Ben Afia (soufian.benafia@haw-hamburg.de),
* Radhuan Alidou (radhuan.alidou@haw-hamburg.de)
* Aufgabe: Aufgabenblatt 1, Aufgabe 2
* 
*/

/** Die Klasse  Pruefungsleistung repr�sentiert die Pr�fungsleistung */

public class Pruefungsleistung {

	private String modulName;
	private int note;
	
	
	public Pruefungsleistung(String modulName, int note)
	{
		this.modulName = modulName;
		this.note = note;
	}
	
	public String getModulName() 
	{
		return modulName;
	}
	
	public void setModulName(String modulName) 
	{
		this.modulName = modulName;
	}
	
	public int getNote()
	{
		return note;
	}
	
	public void setNote(int note) 
	{
		this.note = note;
	}
}

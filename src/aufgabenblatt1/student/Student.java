package aufgabenblatt1.student;

import java.util.*;

/**
* Praktikum TIPR2, SS 2015
* Gruppe: Soufian Ben Afia (soufian.benafia@haw-hamburg.de),
* Radhuan Alidou (radhuan.alidou@haw-hamburg.de)
* Aufgabe: Aufgabenblatt 1, Aufgabe 2
* 
*/

/** Die Klasse  Student repr�sentiert ein Studenten mit Angaben seiner Person */

public class Student implements Comparable<Student>, Comparator<Student>{

	
	private String vorname;
	private String nachname;
	private int matrikelnummer;
	private ArrayList <Pruefungsleistung> listePruefung = new ArrayList<>();
	
	
	

	public Student(String vorname, String nachname, int martikelnummer) {
		this.vorname = vorname;
		this.nachname = nachname;
		this.matrikelnummer = martikelnummer;
	}
	
	
	/** vergleicht Objekte von Student anhand Ihrer Matrikelnummer 
	 * 
	 * @param 	andererStudent  Das Objekt mit dem verglichen wird
	 * 
	 * @return 	eine negative ganze Zahl, Null oder eine positive ganze Zahl, 
	 * 			f�r Objekt kleiner als, gleich oder gr��er als das angegebene Objekt
	 * 
	 * */
	@Override
	public int compareTo(Student andererStudent)
	{

		if((this.matrikelnummer == andererStudent.matrikelnummer))
		{
			return 0;
		}
		else if(this.matrikelnummer < andererStudent.matrikelnummer)
		{
			return -1;
		}
		else
		{
			return 1;
		}
	}


	/** vergleicht Objekte von Student anhand Nachname und vorname 
	 * 
	 * @param o1	Das erste Objekt mit dem verglichen wird
	 * @param o2	Das z Objekt mit dem verglichen wird
	 * 
	 * @return 	eine negative ganze Zahl, Null oder eine positive ganze Zahl, 
	 * 			f�r das erste Argument kleiner als, gleich oder gr��er als die zweite.
	 * */
	
	@Override
	public int compare(Student o1, Student o2) 
	{
		if(o1.nachname.equals(o2.nachname) && (o1.vorname.equals(o2.vorname)))
		{
			return 0;
		}
		else if(o1.nachname.equals(null) && (o1.vorname.equals(null)))
		{
			return -1;
		}
		else
		{
			return 1;
		}
			
	}
	

	public List<Pruefungsleistung> getListePruefung() {
		return listePruefung;
	}

	public void setListePruefung(Pruefungsleistung objekt) {
		this.listePruefung.add(objekt);
	}
	
	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public int getMartikelnummer() {
		return matrikelnummer;
	}

	public void setMartikelnummer(int martikelnummer) {
		this.matrikelnummer = martikelnummer;
	}
}

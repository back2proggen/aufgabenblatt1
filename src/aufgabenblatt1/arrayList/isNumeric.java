package aufgabenblatt1.arrayList;

/**
 * Praktikum TIPR2, SS 2015 Gruppe: Soufian Ben Afia
 * (soufian.benafia@haw-hamburg.de), Radhuan Alidou
 * (radhuan.alidou@haw-hamburg.de) Aufgabe: Aufgabenblatt 1, Aufgabe 4
 * 
 * 
 * /**Die Klasse checkt ob in eine beliebige Liste eine Zahl existiert
 */

public class isNumeric {

	/**
	 * Die Methode checkt ob in eine beliebige Liste eine Zahl existiert
	 * 
	 * @param list
	 *            Liste die abgecheckt werden soll
	 * 
	 */

	public static void inspect(ArrayList list) {

		if (list.getAnzahlElemente() >= 1) {
			if (!(list.getElemente()[0] instanceof Number)) {
				System.out.println("Keine Zahl");
			} else {
				System.out.println("Ist eine Zahl");
			}
		}

	}
}



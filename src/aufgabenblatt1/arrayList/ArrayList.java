package aufgabenblatt1.arrayList;

/**
* Praktikum TIPR2, SS 2015
* Gruppe: Soufian Ben Afia (soufian.benafia@haw-hamburg.de),
* Radhuan Alidou (radhuan.alidou@haw-hamburg.de)
* Aufgabe: Aufgabenblatt 1, Aufgabe 4
* 
*/

/**
 * Die Klasse implementiert eine ArrayList mein ein automatisch vergr��baren Array
 */

public class ArrayList<T extends Comparable<T>> {

	private int anzahlElemente;
	private Object[] elemente;

	public Object[] getElemente() {
		return elemente;
	}

	public void setElemente(Object[] elemente) {
		this.elemente = elemente;
	}

	public ArrayList() {
		elemente = new Object[5];
	}

	/**
	 * f�gt ein Element in der Liste
	 * 
	 * @param element
	 *            Das Element,welches in der Liste hinzuzuf�gen ist
	 */

	public void hinzufuegen(T element) {
		if (anzahlElemente >= elemente.length) {
			Object[] tmp = new Object[anzahlElemente];
			System.arraycopy(elemente, 0, tmp, 0, anzahlElemente);
			elemente = new Object[anzahlElemente + 1];
			System.arraycopy(tmp, 0, elemente, 0, tmp.length);
			elemente[anzahlElemente] = element;
			anzahlElemente++;
		} else {
			elemente[anzahlElemente] = element;
			anzahlElemente++;
		}

	}

	/**
	 * holen ein Element aus der Liste
	 * 
	 * @param index
	 *            Die Position des Element
	 */
	public T get(int index) {
		return (T) elemente[index];
	}

	/**
	 * entfernt ein Element aus der Liste
	 * 
	 * @param element
	 *            Das Element,welches in der Liste zu entfernen ist
	 */

	public void entfernen(T element) {
		for (int i = 0; i <= anzahlElemente; i++) {
			if (element.equals(elemente[i])) {
				elemente[i] = null;
				anzahlElemente--;

				for (int j = i; j <= anzahlElemente; j++) {
					elemente[j] = elemente[j + 1];
				}
			}

		}

	}

	/**
	 * entfernt ein Element aus der Liste anhand der Position
	 * 
	 * @param index
	 *            Die Position des Elementes
	 */

	public void entferneElementArtIndex(int index) {
		elemente[index] = null;
		anzahlElemente--;

		for (int j = index; j <= anzahlElemente; j++) {
			elemente[j] = elemente[j + 1];
		}

	}

	/**
	 * gibt die Anzahl der Elemente zur�ck
	 * 
	 * @return anzahlElemente die Anzahl der Elemente im Array
	 */

	public int getAnzahlElemente() {
		return anzahlElemente;
	}

	/**
	 * berechnet die Summe der Elemente
	 * 
	 * @return summe Die Summe der Elemente
	 */

	public int berechneSumme() {
		int summe = 0;

		for (int i = 0; i < anzahlElemente; i++) {

			summe = summe + (Integer) (elemente[i]);

		}
		return summe;
	}

	/**
	 * holt den kleinsten Element aus der Liste raus
	 * 
	 * @return min Das kleinste Element
	 */
	public T getKleinstesElement() {

		int pos = 0;
		int j = 0;
		T min = (T) elemente[pos];

		for (int i = 1; i < anzahlElemente; i++) {

			j = min.compareTo((T) elemente[i]);

			if (j == 1) {
				min = (T) elemente[i];
			}
		}

		return min;
	}

}
